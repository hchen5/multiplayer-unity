using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using System.Diagnostics;
using System;
using Debug = UnityEngine.Debug;
using Unity.Netcode.Components;
using UnityEngine.SceneManagement;

public class PlayerNetwork : NetworkBehaviour
{
    private List<GameObject> list = new List<GameObject>();
    public NetworkVariable<int> m_Score = new NetworkVariable<int>(0);
    NetworkVariable<float> m_Speed = new NetworkVariable<float>(10);
    Rigidbody2D rb ;
    Vector3 movement;
    private new CameraSeguir m_Camera;

    private bool m_IsAlive = true;
    private GameObject m_MyBullet;
    public GameObject bulletPrefab;
    //[SerializeField] public GameObject project;

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        
        if (!IsOwner) return;
        //m_Camera = FindObjectOfType<CameraSeguir>();
        //m_Camera.Seguir(transform);
        m_Score.OnValueChanged += ChangeScore;
        m_Speed.OnValueChanged += CallbackModificacio;

    }
    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        m_Speed.OnValueChanged -= CallbackModificacio;

    }
    private void CallbackModificacio(float oldValue, float newValue)
    {
        Debug.Log(string.Format("{0} => M'han modificat la velocitat i ara s {1} en comptes de {2}", OwnerClientId, newValue, oldValue));
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        
        if (!IsOwner || !IsLocalPlayer )
            return;
        
            if (Input.GetKeyDown(KeyCode.M))
                SendMessageClientRpc("Aix s un missatge pels clients");

            movement = Vector3.zero;
            if (Input.GetKey(KeyCode.A))
                movement -= Vector3.right;
            if (Input.GetKey(KeyCode.D))
                movement += Vector3.right;
            if (Input.GetKeyDown(KeyCode.Space)) 
                ShootServerRPC();
        //MoveCharacterPhysicsServerRpc(movement.normalized * m_Speed.Value);
        MovePlayer(movement.normalized * m_Speed.Value);

    }
    [ClientRpc]
    private void SendMessageClientRpc(string message, ClientRpcParams clientRpcParams = default)
    {
        Debug.Log(string.Format("{0} => {1}", OwnerClientId, message));
    }
    private void MovePlayer(Vector3 velocity) 
    {
        rb.velocity = velocity;
    }
    [ServerRpc]
    private void ShootServerRPC()
    {
        if (!m_IsAlive)
            return;

        if (m_MyBullet == null)
        {
            m_MyBullet = Instantiate(bulletPrefab, transform.position + Vector3.up, Quaternion.identity);
            m_MyBullet.GetComponent<Bullet>().owner = this;
            m_MyBullet.GetComponent<NetworkObject>().Spawn();
            //Destroy(m_MyBullet,2f);
        }
    }
    [ServerRpc(RequireOwnership =false)]
    private void MoveCharacterPhysicsServerRpc(Vector3 velocity, ServerRpcParams serverRpcParams = default)
    {
        rb.velocity = velocity;
        Debug.Log(serverRpcParams);
    }
    public void ChangeScore(int previousAmount, int currentAmount) 
    {
        if(!IsOwner) return;
        if (Manag.Singleton != null) Manag.Singleton.SetScore(m_Score.Value);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!IsOwner) return;
            if (collision.tag == "enemic")
            {
           
                gameObject.SetActive(false);
                SceneManager.LoadScene("GameOver");
            
            }
    }
}
