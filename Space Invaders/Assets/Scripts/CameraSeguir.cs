using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSeguir : MonoBehaviour
{
    
    private Transform m_Seguir;

    public void Seguir(Transform seguir)
    {
        m_Seguir = seguir;
    }
    private void Update()
    {
        if(m_Seguir)
        transform.position = m_Seguir.position * 10f;
    }

}
