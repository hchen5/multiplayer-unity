using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

public class Bullet : MonoBehaviour
{
    public PlayerNetwork owner;

    [SerializeField]
    private float m_TravelSpeed = 4.0f;

    private void Update()
    {
        if (!NetworkManager.Singleton.IsServer) return;

        transform.Translate(0, m_TravelSpeed * Time.deltaTime, 0);
        
            
    }
    private void OnBecameInvisible()
    {
        if (NetworkManager.Singleton.IsServer)
            Destroy(gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!NetworkManager.Singleton.IsServer)
            return;
        if (collision.tag=="enemic") 
        {   
            Destroy(collision.gameObject);
            owner.ChangeScore(owner.m_Score.Value,owner.m_Score.Value+=1);
        }
    }
}
