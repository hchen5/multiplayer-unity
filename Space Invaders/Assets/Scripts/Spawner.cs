using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject m_ElementASpawnejar;
    [SerializeField]
    private float m_SpawnRate = 3f;
    private float m_SpawnRateDelta = 3f;

    [SerializeField]
    private Transform[] m_SpawnPoints;

    void Start()
    {
        //m_SpawnRateDelta = m_SpawnRate;

        StartCoroutine(SpawnCoroutine());
        StartCoroutine(IncreaseDifficulty());
    }


    IEnumerator SpawnCoroutine()
    {
        while(true)
        {
            GameObject spawned = Instantiate(m_ElementASpawnejar);
            spawned.transform.position = m_SpawnPoints[Random.Range(0,m_SpawnPoints.Length)].position;
            yield return new WaitForSeconds(m_SpawnRate);
        }
    }

    IEnumerator IncreaseDifficulty()
    {
        while(true)
        {
            yield return new WaitForSeconds(2);
            m_SpawnRate -= 0.4f;

            if (m_SpawnRate <= 0.4f)
                m_SpawnRate = 0.4f;
        }
        
    }


}
