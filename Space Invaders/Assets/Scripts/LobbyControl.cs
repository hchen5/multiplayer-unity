﻿using System;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class LobbyControl : NetworkBehaviour
{
    [SerializeField]
    private string m_NomEscena = "InGame";
    
    [SerializeField]
    private int m_JugadorsMinims = 2;
    
    public TMP_Text LobbyText;
    private bool m_TotsPlayersLobby;

    private Dictionary<ulong, bool> m_ClientesLobby;
    private string m_ClientStatusText;

    public override void OnNetworkSpawn()
    {
        m_ClientesLobby = new Dictionary<ulong, bool>();
        
        m_ClientesLobby.Add(NetworkManager.LocalClientId, false);

        if (IsServer)
        {
            m_TotsPlayersLobby = false;

            NetworkManager.OnClientConnectedCallback += OnClientConnectedCallback;
            SceneTransitionHandler.sceneTransitionHandler.OnClientLoadedScene += ClientLoadedScene;
        }

        StatTextLobby();

        SceneTransitionHandler.sceneTransitionHandler.SetSceneState(SceneTransitionHandler.SceneStates.Lobby);
    }

    private void OnGUI()
    {
        if (LobbyText != null) LobbyText.text = m_ClientStatusText;
    }

    private void StatTextLobby()
    {
        m_ClientStatusText = string.Empty;
        foreach (var clientLobbyStatus in m_ClientesLobby)
        {
            m_ClientStatusText += "PLAYER " + clientLobbyStatus.Key + "          ";
            if (clientLobbyStatus.Value)
                m_ClientStatusText += "READY\n";
            else
                m_ClientStatusText += "NOT READY\n";
        }
    }

    private void UpdateAndCheckPlayersInLobby()
    {
        m_TotsPlayersLobby = m_ClientesLobby.Count >= m_JugadorsMinims;

        foreach (var clientLobbyStatus in m_ClientesLobby)
        {
            SendClientReadyStatusUpdatesClientRpc(clientLobbyStatus.Key, clientLobbyStatus.Value);
            if (!NetworkManager.Singleton.ConnectedClients.ContainsKey(clientLobbyStatus.Key))

                m_TotsPlayersLobby = false;
        }

        ComprobarTotsJugadorsReady();
    }

    private void ClientLoadedScene(ulong clientId)
    {
        if (IsServer)
        {
            if (!m_ClientesLobby.ContainsKey(clientId))
            {
                m_ClientesLobby.Add(clientId, false);
                StatTextLobby();
            }

            UpdateAndCheckPlayersInLobby();
        }
    }

    private void OnClientConnectedCallback(ulong clientId)
    {
        if (IsServer)
        {
            if (!m_ClientesLobby.ContainsKey(clientId)) m_ClientesLobby.Add(clientId, false);
            StatTextLobby();

            UpdateAndCheckPlayersInLobby();
        }
    }

    [ClientRpc]
    private void SendClientReadyStatusUpdatesClientRpc(ulong clientId, bool isReady)
    {
        if (!IsServer)
        {
            if (!m_ClientesLobby.ContainsKey(clientId))
                m_ClientesLobby.Add(clientId, isReady);
            else
                m_ClientesLobby[clientId] = isReady;
            StatTextLobby();
        }
    }
    private void ComprobarTotsJugadorsReady()
    {
        if (m_TotsPlayersLobby)
        {
            var allPlayersAreReady = true;
            foreach (var clientLobbyStatus in m_ClientesLobby)
                if (!clientLobbyStatus.Value)

                    allPlayersAreReady = false;


            if (allPlayersAreReady)
            {
             
                NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnectedCallback;

                
                SceneTransitionHandler.sceneTransitionHandler.OnClientLoadedScene -= ClientLoadedScene;

              
                SceneTransitionHandler.sceneTransitionHandler.SwitchScene(m_NomEscena);
            }
        }
    }

    
    public void JugadorsPreparats()
    {
        m_ClientesLobby[NetworkManager.Singleton.LocalClientId] = true;
        if (IsServer)
        {
            UpdateAndCheckPlayersInLobby();
        }
        else
        {
            OnClientIsReadyServerRpc(NetworkManager.Singleton.LocalClientId);
        }

        StatTextLobby();
    }
    
    [ServerRpc(RequireOwnership = false)]
    private void OnClientIsReadyServerRpc(ulong clientid)
    {
        if (m_ClientesLobby.ContainsKey(clientid))
        {
            m_ClientesLobby[clientid] = true;
            UpdateAndCheckPlayersInLobby();
            StatTextLobby();
        }
    }
}
